<?php

namespace App\Http\Controllers;

use App\Models\groupMembres;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class GroupMembresController extends Controller
{
    public function isInGroup(Request $request)
    {
        $user_id= Auth::id();
        $user2_id=$request->id;
        $Me=  DB::table('group_membres')
        ->where('id_user',  $user_id)
        ->where('id_groupe', $request->id)
        ->get();
     
        
        if (!$Me->isEmpty())
        {
            return response()->json(['membre']);
        } else
        {
            return response()->json(['non']);    
        }
       
    }

    public function AddMembre(Request $request)
    {
        $user_id= Auth::id();
        $groupMembres =new groupMembres;
        $groupMembres->id_user =$user_id    ;
        $groupMembres->id_groupe =$request[0] ;
        $groupMembres->statut ="membre" ;
        $groupMembres->save();
   return response()->json();
    }

    public function RemoveMembre(Request $request)
    {
        $user_id= Auth::id();
      groupMembres::where('id_user', $user_id)->where('id_groupe', $request[0])->delete();
 
      return response()->json($request[0]);
    }
}