<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class userManagement extends Controller
{
    public function getGusers()
    {
        $user = DB::table('users')
        ->leftJoin('utilisateurs', 'users.id', '=', 'utilisateurs.user_id')
        ->select('utilisateurs.image_profile', 'users.name', 'users.email', 'utilisateurs.telephone',
         'users.created_at', 'users.statut', 'users.verification')
        ->get();
        return response()->json($user);
    }
}
