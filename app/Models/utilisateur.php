<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class utilisateur extends Model
{
    use HasFactory;
    protected $fillable = [
        'id',
        'user_id',
        'adresse',
        'date_naissance',
        'description',
        'image_couverture',
        'image_profile',
        'maladie',
        'nationnalite',
        'telephone',
        'poid',
        'sexe',
        'taille',
        'pays',
    ];
}
