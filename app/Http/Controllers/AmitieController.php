<?php

namespace App\Http\Controllers;

use App\Models\amitie;
use App\Models\notification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class AmitieController extends Controller
{
    public function show(Request $request)
    {
        $user_id= Auth::id();
        $friends = DB::table('amities')
        ->leftJoin('users', 'amities.user1_id', '=', 'users.id')
        ->leftJoin('utilisateurs', 'amities.user1_id', '=', 'utilisateurs.id')
        ->select('amities.*', 'users.name', 'utilisateurs.image_profile')
        ->where('amities.user2_id',$user_id)
        ->where('amities.statut',"en-attente")
        ->orderBy('id', 'desc')
        ->take(3)
        ->get();
        return response()->json($friends);
    }

    public function accept(Request $request)
    {
   amitie::whereIn('id', $request)->update(['statut' => 'amie']);
   $amie=amitie::find($request);
  
   $notification =new notification;
   $notification->id_user1 =$amie[0]->user2_id     ;
   $notification->id_user2  =$amie[0]->user1_id  ; 
   $notification->notification ='accepted you request' ; 
   $notification->type ='accept' ; 
   $notification->vu ='non' ; 
   $notification->save();

   
   return response()->json($amie[0]->user1_id);
    }
    public function Delete(Request $request)
    {
   amitie::whereIn('id', $request)->update(['statut' => 'Refuser']);
   return response()->json();
    }
    public function Add(Request $request)
    {
        $user_id= Auth::id();
        $user =new amitie;
        $user->user1_id =$user_id    ;
        $user->user2_id =$request[0] ;
        $user->statut ="en-attente" ;
        $user->save();

   $notification =new notification;
   $notification->id_user1 =$user_id    ;
   $notification->id_user2  =$request[0] ; 
   $notification->notification ='sent you request' ; 
   $notification->type ='invit' ; 
   $notification->vu ='non' ; 
   $notification->save();
   return response()->json();
    }
    public function isFriend(Request $request)
    {
        $user1_id= Auth::id();
        $user2_id=$request->id;
        $Me=  DB::table('amities')
        ->where('user1_id', $user1_id)
        ->where('user2_id', $user2_id)
        ->get();
        $He=  DB::table('amities')
        ->where('user1_id',$user2_id)
        ->where('user2_id',$user1_id)
        ->get();
        
        if (!$Me->isEmpty())
        {
            return response()->json([
                'Me' =>$Me[0],
               'stat'=> 'Me' ,
            ]);
        }else if (!$He->isEmpty())
        {
            return response()->json([
                'Me' =>$He[0],
               'stat'=> 'you' ,
            ]);
        }else if(($Me->isEmpty())&&($He->isEmpty()))
        {
            return response()->json([
                'Me' =>"Me",
               'stat'=> 'non' ,
            ]);
        }
    }
    public function deleteRequestFriend(Request $request)
    {
        amitie::find($request[0])->delete();
      //  amitie::where('id', $request)->delete();
     // amitie::destroy($request);
      return response()->json($request[0]);
    }
}
