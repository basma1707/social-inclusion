<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class event extends Model
{
    use HasFactory;
    protected $fillable = [
        'id_user',
        'nom',
        'date',
        'description',
        'image',
        'latitude',
        'longitude'
    ];
}
