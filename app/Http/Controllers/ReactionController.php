<?php

namespace App\Http\Controllers;

use App\Models\reaction;
use App\Models\notification;
use App\Models\publications;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ReactionController extends Controller
{
    public function AddReact(Request $request)
    {
        $user_id= Auth::id();
        $reaction =new reaction;
        $reaction->user1_id =$user_id    ;
        $reaction->Type =$request->emoji ; 
        $reaction->publication_id =$request->publicationId ; 
        $reaction->save();

        $pub=publications::find($request->publicationId);
  if($pub->user_id !=$user_id)
  {
    $notification =new notification;
    $notification->id_user1 =$user_id;     ;
     $notification->id_user2  =$pub->user_id  ; 
     $notification->notification ='reacted your publication' ; 
     $notification->type ='react' ; 
     $notification->vu ='non' ; 
     $notification->idpub =$request->publicationId  ; 
     $notification->save();
  }
       
   return response()->json();
    }

    public function UpdateReact(Request $request)
    {
        $user_id = Auth::id();
        $publication_id = $request->publicationId;
        reaction::where('user1_id', $user_id)
                ->where('publication_id', $publication_id)
                ->update(['Type' => $request->emoji]);
        return response()->json();
    }
    public function DeleteReact(Request $request)
    {
        $user_id = Auth::id();
        $publication_id = $request->publicationId;
        reaction::where('user1_id', $user_id)
                ->where('publication_id', $publication_id)
                ->delete();
      return response()->json();
    }
}
