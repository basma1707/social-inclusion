<?php

namespace App\Http\Controllers;

use App\Models\notification;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class NotificationController extends Controller
{
    public function showall()
    {
        $user_id = Auth::id();
$user = DB::table('notifications')
    ->leftJoin('users', 'users.id', '=', 'notifications.id_user1')
    ->leftJoin('utilisateurs', 'utilisateurs.id', '=', 'notifications.id_user1')
    ->select('utilisateurs.image_profile', 'users.name', 'notifications.*')
    ->where('id_user2',$user_id)
    ->orderBy('created_at', 'desc')
    ->get();

$count_non_vu = $user->sum(function ($notification) {
    return ($notification->vu == 'non') ? 1 : 0;
});

foreach ($user as $notification) {
    $notification_created_at = Carbon::createFromFormat('Y-m-d H:i:s', $notification->created_at);
    $diff_in_minutes = $notification_created_at->diffInMinutes();
    $diff_in_hours = $notification_created_at->diffInHours();
    $diff_in_days = $notification_created_at->diffInDays();

    if ($diff_in_days > 6) {
        $notification->time_diff_human = $notification_created_at->format('j F Y');
    } elseif ($diff_in_minutes < 60) {
        $notification->time_diff_human = $diff_in_minutes . ' min ago';
    } elseif ($diff_in_hours < 24) {
        $notification->time_diff_human = $diff_in_hours . ' h ago';
    } else {
        $notification->time_diff_human = $diff_in_days . ' days ago';
    }
}

return response()->json([
    'notifications' => $user,
    'count_non_vu' => $count_non_vu
]);
}

public function vu(Request $request)
{
    notification::whereIn('id', $request)->update(['vu' => 'oui']);
return response()->json();
}
}
