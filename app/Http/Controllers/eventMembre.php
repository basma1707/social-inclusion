<?php

namespace App\Http\Controllers;

use App\Models\event_membres;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class eventMembre extends Controller
{
    public function isInEvent(Request $request)
    {
        $user_id= Auth::id();
        $user2_id=$request->id;
        $Me=  DB::table('event_membres')
        ->where('id_user',  $user_id)
        ->where('id_events', $request->id)
        ->get();
     
        
        if (!$Me->isEmpty())
        {
            return response()->json(['membre']);
        } else
        {
            return response()->json(['non']);    
        }
       
    }

    public function AddMembreEvent(Request $request)
    {
        $user_id= Auth::id();
        $groupMembres =new event_membres;
        $groupMembres->id_user =$user_id    ;
        $groupMembres->id_events =$request[0] ;
        $groupMembres->statut ="membre" ;
        $groupMembres->save();
   return response()->json();
    }

    public function RemoveMembreEvent(Request $request)
    {
        $user_id= Auth::id();
        event_membres::where('id_user', $user_id)->where('id_events', $request[0])->delete();
 
      return response()->json($request[0]);
    }
}
