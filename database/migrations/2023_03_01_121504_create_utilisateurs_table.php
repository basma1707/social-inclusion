<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUtilisateursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('utilisateurs', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->string('adresse',50);
            $table->date('date_naissance');
            $table->longText('description')->nullable();
            $table->string('image_couverture')->nullable();
            $table->string('image_profile')->nullable();
            $table->string('maladie')->nullable();
            $table->string('nationnalite',50);
            $table->string('telephone');
            $table->float('poid');
            $table->set('sexe', ['m', 'f']);
            $table->float('taille');
            $table->string('pays',50);
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');;
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('utilisateurs');
    }
}
