<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\utilisateur;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class UtilisateurController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' =>  'required',
            'telephone' =>  'required|numeric',
            'date_naissance' =>  'required',
            'sexe' =>  'required',
            'nationnalite' =>  'required|max:20',
            'pays' =>  'required|max:20',
            'adresse' =>  'required|max:20',
            'taille' =>  'required|numeric',
            'poid' =>  'required|numeric',
            'maladie' =>  'max:50',
            'description' => 'max:350',

        ]);
        if($validator->fails()){
            return response()->json(['error' =>$validator->errors()],400);
         }
         $imageName=null;
         $imageName1=null;
         if($request->file('image')!=null)
         {
            $image = $request->file('image');
            $imageName = uniqid();
            $imageName.= '.' . $image->getClientOriginalExtension();
            $image->move(public_path('images/couverture'), $imageName);
         }
         if($request->file('image1')!=null)
         {
            $image1 = $request->file('image1');
            $imageName1 = uniqid();
            $imageName1.= '.' . $image1->getClientOriginalExtension();
            $image1->move(public_path('images/profil'), $imageName1);
         }

         $utilisateur = utilisateur::create(array_merge(
            $validator->validate(),
            ['user_id' => $request->id,'image_couverture' => $imageName,'image_profile' => $imageName1]
        ));
        $user = User::find($request->id);
        $user->verification="1";
        $user->update();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\utilisateur  $utilisateur
     * @return \Illuminate\Http\Response
     */
    public function show(utilisateur $utilisateur)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\utilisateur  $utilisateur
     * @return \Illuminate\Http\Response
     */
    public function edit(utilisateur $utilisateur)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\utilisateur  $utilisateur
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, utilisateur $utilisateur)
    {
        $id=$request->id;
        $utilisateur = utilisateur::find($id);
        $validator = Validator::make($request->all(), [
            'id' =>  'required',
            'telephone' =>  'required|numeric',
            'date_naissance' =>  'required|date',
            'sexe' =>  'required',
            'nationnalite' =>  'required|max:20',
            'pays' =>  'required|max:20',
            'adresse' =>  'required|max:20',
            'taille' =>  'required|numeric',
            'poid' =>  'required|numeric',
            'maladie' =>  'max:50',
            'description' => 'max:350',

        ]);
        if($validator->fails()){
            return response()->json(['error' =>$validator->errors()],400);
         }
         if($request->file('image')!=null)
         {
            $image = $request->file('image');
            if($utilisateur->image_couverture!=null)
            {
            $file_path = public_path().'/images/couverture/'.$utilisateur->image_couverture;
            unlink($file_path);
            }
          
            $imageName = uniqid();
            $imageName.= '.' . $image->getClientOriginalExtension();
            $image->move(public_path('images/couverture'), $imageName);
            $utilisateur->image_couverture=$imageName;
        }
        if($request->file('image1')!=null)
        {
            $image1 = $request->file('image1');
            if($utilisateur->image_profile!=null)
            {
            $file_path = public_path().'/images/profil/'.$utilisateur->image_profile;
            unlink($file_path);
            }
            $imageName1 = uniqid();
            $imageName1.= '.' . $image1->getClientOriginalExtension();
            $image1->move(public_path('images/profil'), $imageName1);
            $utilisateur->image_profile=$imageName1;
        }
         
         $utilisateur->adresse=$request->adresse;
         $utilisateur->date_naissance =$request->date_naissance ;
         $utilisateur->description=$request->description;
         $utilisateur->maladie=$request->maladie;
         $utilisateur->nationnalite=$request->nationnalite;
         $utilisateur->telephone=$request->telephone;
         $utilisateur->poid=$request->poid;
         $utilisateur->sexe=$request->sexe;
         $utilisateur->taille=$request->taille;
         $utilisateur->pays=$request->pays;
         $utilisateur->update();

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\utilisateur  $utilisateur
     * @return \Illuminate\Http\Response
     */
    public function destroy(utilisateur $utilisateur)
    {
        //
    }
    public function showProfileUser(Request $request)
    {
      
        $user = DB::table('users')
        ->leftJoin('utilisateurs', 'users.id', '=', 'utilisateurs.user_id')
        ->select('utilisateurs.*', 'users.name','users.email')
        -> where('utilisateurs.user_id', $request->id)
        ->get();
        if ($user->isEmpty())
        {
            return response()->json(['error']);
        }
        else
        {
            return response()->json($user);
        }
        
    }
    public function search(Request $request)
    {
        $searchText = $request->input('q');
        $searchResults = DB::table('users')->where('name', 'like', "%$searchText%")
        -> where('role', "user") 
        ->leftJoin('utilisateurs' , 'utilisateurs.id', '=', 'users.id')
        ->get();

        return response()->json($searchResults);
        
    }

    public function GetContact(Request $request)
    {
        $users = DB::table('users')
        ->leftJoin('utilisateurs', 'users.id', '=', 'utilisateurs.id')
        ->select('utilisateurs.image_profile', 'users.*')
        ->get();
        return response()->json($users);
    } 
}
