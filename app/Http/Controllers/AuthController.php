<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login', 'register',]]);
    }

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        $credentials = request(['email', 'password']);

        if (! $token = auth()->attempt($credentials)) {
            return response()->json(['error' => 'Email or password does\'t exist'], 401);
        }
        $user = Auth::user();
     return $this->respondWithToken($token ,$request);
      //  $name = DB::table('users')->where('email', $request->email)->first();
       //return  response()->json(['token' => $this->respondWithToken($token),'name' => $name->name]);
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me(Request $request)
   
    {$token = $request->header('Authorization');
        if (preg_match('/Bearer\s(\S+)/', $token, $matches)) {
            $jwt_token = $matches[1];
            // Authentification de l'utilisateur avec le jeton JWT
            $user = auth()->user();
        $utilisateur = DB::table('utilisateurs')->where('user_id', $user->id)->first();
            return response()->json(
                ['id' => $user->id,
                'name' => $user->name,
                'email' => $user->email,
                'image_profile' => $utilisateur->image_profile,
                'image_couverture' => $utilisateur->image_couverture,
                'role' => $user->role,
                'statut' => $user->statut,
                'adresse' => $utilisateur->adresse,
                'date_naissance' => $utilisateur->date_naissance,
                'description' => $utilisateur->description,
                'maladie' => $utilisateur->maladie,
                'nationnalite' => $utilisateur->nationnalite,
                'telephone' => $utilisateur->telephone,
                'poid' => $utilisateur->poid,
                'sexe' => $utilisateur->sexe,
                'taille' => $utilisateur->taille,
                'pays' => $utilisateur->pays,

            ]);


        
        }
        else{
            return response()->json('error',400);
        }
   
       
       
        
    }
    public function getusers(Request $request)
   
    {$token = $request->header('Authorization');
        if (preg_match('/Bearer\s(\S+)/', $token, $matches)) {
            $jwt_token = $matches[1];
            // Authentification de l'utilisateur avec le jeton JWT
            $user = auth()->user();
            return response()->json($user);        
        }
        else{
            return response()->json('error',400);
        }
    }
    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth()->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh(Request $request)
    {   
        $token = $request->header('Authorization');
        if (preg_match('/Bearer\s(\S+)/', $token, $matches)) {
            $jwt_token = $matches[1];
            // Authentification de l'utilisateur avec le jeton JWT
            return $this->respondWithToken(auth()->refresh());
        }
        else{
            return response()->json('error');
        }
        
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
       // $role = DB::table('users')->where('email', $request->email)->first();
        return response()->json([
            'id' =>auth()->user(),
           'role'=> auth()->user()->role ,
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60 ,
        ]);
    }

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|min:4',
            'role' => 'required',
            'email' => 'required|string|email|max:100|unique:users',
            'password' => 'required|confirmed|string|min:6',
        ]);
        if($validator->fails()){
           return response()->json(['error' =>$validator->errors()],400);
           //  return response()->json(['error' => "$message"], 401);
          //  return response()->json(['error' => 'Unauthorized'], 401);
        }

        $user = User::create(array_merge(
            $validator->validate(),
            ['password' => bcrypt($request->password)]
        ));

        return $this->login($request);
    }

    public function updatePassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'current' => 'required',
            'password' => 'required|confirmed|min:6',
            
        ]);
        if($validator->fails()){
           return response()->json(['error' =>$validator->errors()],400);
        }
     $id=auth()->user();
     $user = User::find($id->id);

     if (Hash::check($request->current,$user->password))
    {
        if (Hash::check($request->password,$user->password))
        {
            $data2 = ['error' => 'The new password is already the old one !'];
            return response()->json(['error'=>$data2 ],400);
        }
    $user->password = bcrypt($request->password);
    $user->update();
    return response()->json(['data'=>'Password Successfully Changed']);
    }
    $data2 = ['error' => 'Current Password not fund !'];
    return response()->json(['error'=>$data2 ],400);
    }

}