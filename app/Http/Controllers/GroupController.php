<?php

namespace App\Http\Controllers;

use App\Models\group;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class GroupController extends Controller
{
    public function AddGroup(Request $request)
    {
        $user_id = Auth::id();
        $validator = Validator::make($request->all(), [
     
            'nom' =>  'required|max:20',
            'description' => 'max:350',

        ]);
        if($validator->fails()){
            return response()->json(['error' =>$validator->errors()],400);
         }
         $imageName=null;
         if($request->file('image')!=null)
         {
            $image = $request->file('image');
            $imageName = uniqid();
            $imageName.= '.' . $image->getClientOriginalExtension();
            $image->move(public_path('images/group'), $imageName);
         }
      
         $utilisateur = group::create(array_merge(
            $validator->validate(),
            ['image' => $imageName,'id_user'=>$user_id]
        ));

    }

    public function Showgroupe(Request $request)
    {
      
        $user = DB::table('groups')
        -> where('id', $request->id)
        ->get();
        if ($user->isEmpty())
        {
            return response()->json(['error']);
        }
        else
        {
            return response()->json($user);
        }
        
    }
    public function getGroupinformation(Request $request)
    {
        $group = DB::table('groups')
        ->where('id', $request->id)
        ->get();
        return response()->json($group);
    }

    public function UpdateGroup(Request $request)
    {
        $user_id = Auth::id();
        $group = group::find($id);
        $validator = Validator::make($request->all(), [
     
            'nom' =>  'required|max:20',
            'description' => 'max:350',

        ]);
        if($validator->fails()){
            return response()->json(['error' =>$validator->errors()],400);
         }
         $imageName=null;
         if($request->file('image')!=null)
         {
            $image = $request->file('image');
            $imageName = uniqid();
            $imageName.= '.' . $image->getClientOriginalExtension();
            $image->move(public_path('images/group'), $imageName);
         }
      
      

    }

    public function getGroups(Request $request)
    {
        $group = DB::table('groups')
        ->get();
        return response()->json($group);
    }
}
