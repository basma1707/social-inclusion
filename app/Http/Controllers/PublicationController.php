<?php
namespace App\Http\Controllers;
use App\Models\images;
use App\Models\publications;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;

class PublicationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function Add(Request $request )
    {
        $user_id= Auth::id();   
        $publication =new publications;
        $publication->user_id =$user_id ;
        $publication->texte =$request->texte ;
        $publication->save();
        $idLasPub = publications::select('*')->orderBy('id','desc')->first();
        if ($request->hasFile('images')) {
            $images = $request->file('images');
    
            foreach ($images as $image) {
                $imageName = uniqid();
                $imageName.= '.' . $image->getClientOriginalExtension();
                $image->move(public_path('images/publication'), $imageName);
                $photos = new images;
                $photos->pub_id =$idLasPub->id ;
                $photos->image =$imageName ;
                $photos->save();
            }
        }    
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Publication  $publication
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {$userId = Auth::id();
        $publications = DB::table('publications as p')
        ->leftJoin('users as us', 'p.user_id', '=', 'us.id')
        ->leftJoin('utilisateurs', 'p.user_id', '=', 'utilisateurs.id')
        ->leftJoin('images as i', 'p.id', '=', 'i.pub_id')
        ->leftJoin('reactions as r', 'p.id', '=', 'r.publication_id')
        ->leftJoin('users as liker', 'r.user1_id', '=', 'liker.id')
        ->leftJoin('commentaires as c', 'p.id', '=', 'c.publication_id')
        ->leftJoin('users as commenter', 'c.user_id', '=', 'commenter.id')
        ->leftJoin('utilisateurs as u2', 'c.user_id', '=', 'u2.id')
        ->leftJoin('utilisateurs as u1', 'r.user1_id', '=', 'u1.id')
        ->leftJoin(DB::raw('(SELECT publication_id,  
                 CASE WHEN user1_id = '.$userId.'. AND type != "null" THEN type ELSE 0 END as user_liked
          FROM reactions) as ul'), 'p.id', '=', 'ul.publication_id')
        ->groupBy('utilisateurs.image_profile', 'p.id', 'p.texte', 'us.name', 'p.created_at')
        ->orderByDesc('p.id')
        ->select('utilisateurs.image_profile', 'us.id as id_user',
                 'p.id', 
                 'p.texte', 
                 'us.name',
                 'p.created_at',
                 DB::raw('COUNT(DISTINCT r.id) as likes'),
                 DB::raw('COUNT(DISTINCT c.id) as comments_count'),
                 DB::raw('COUNT(DISTINCT i.id) as images_count'),
                 DB::raw('JSON_OBJECTAGG(IFNULL(i.id, \'\'), IFNULL(i.image, \'\')) as images'),
                 DB::raw('GROUP_CONCAT(DISTINCT CONCAT(u1.image_profile, \':\', liker.name, \':\', r.type)) as reactions'),
                 DB::raw('GROUP_CONCAT(DISTINCT r.type) as reaction_types'),
                 DB::raw('JSON_OBJECTAGG(IFNULL(c.id, \'\'), CONCAT(u2.image_profile,  \':\', commenter.name, \':\', commenter.id, \':\', c.texte)) as comments'),
                 DB::raw('MAX(IFNULL(ul.user_liked, \'\')) as user_liked'))
        ->get();
        foreach ($publications as $publication) {
            $publication_created_at = Carbon::createFromFormat('Y-m-d H:i:s', $publication->created_at);
            $diff_in_days = $publication_created_at->diffInDays();
        
            // Vérifie si la publication a été créée il y a plus de 5 jours
            if ($diff_in_days > 5) {
                $publication->time_diff_human = $publication_created_at->format('j F Y');
            } else {
                $publication->time_diff_human = $publication_created_at->diffForHumans();
            }
        }
        
        return response()->json($publications);
            
 }
    public function ShowPublicationProfile(Request $request)
    {
        $id=Auth::id();
        $publications = DB::table('publications')
        ->leftJoin('images', 'publications.id', '=', 'images.pub_id')
        ->leftJoin('users', 'publications.user_id', '=', 'users.id')
        ->leftJoin('utilisateurs', 'publications.user_id', '=', 'utilisateurs.id')
        ->select('publications.*', 'images.image','users.name','utilisateurs.image_profile')
        ->where('publications.user_id', $id)
        ->get()
        ->groupBy('id');
        return response()->json($publications);
    }
    public function ShowPublicationProfileuser(Request $request)
    {
        $publications = DB::table('publications')
        ->leftJoin('images', 'publications.id', '=', 'images.pub_id')
        ->leftJoin('users', 'publications.user_id', '=', 'users.id')
        ->leftJoin('utilisateurs', 'publications.user_id', '=', 'utilisateurs.id')
        ->select('publications.*', 'images.image','users.name','utilisateurs.image_profile')
         ->where('publications.user_id', $request->id)
         ->get()
        ->groupBy('id');
        return response()->json($publications);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Publication  $publication
     * @return \Illuminate\Http\Response
     */
    public function edit(Publication $publication)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Publication  $publication
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Publication $publication)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Publication  $publication
     * @return \Illuminate\Http\Response
     */
    public function destroy(Publication $publication)
    {
        //
    }


    public function PostNotification(Request $request)
    {$userId = Auth::id();
        $publications = DB::table('publications as p')
        ->leftJoin('users as us', 'p.user_id', '=', 'us.id')
        ->leftJoin('utilisateurs', 'p.user_id', '=', 'utilisateurs.id')
        ->leftJoin('images as i', 'p.id', '=', 'i.pub_id')
        ->leftJoin('reactions as r', 'p.id', '=', 'r.publication_id')
        ->leftJoin('users as liker', 'r.user1_id', '=', 'liker.id')
        ->leftJoin('commentaires as c', 'p.id', '=', 'c.publication_id')
        ->leftJoin('users as commenter', 'c.user_id', '=', 'commenter.id')
        ->leftJoin('utilisateurs as u2', 'c.user_id', '=', 'u2.id')
        ->leftJoin('utilisateurs as u1', 'r.user1_id', '=', 'u1.id')
        ->leftJoin(DB::raw('(SELECT publication_id,  
                 CASE WHEN user1_id = '.$userId.'. AND type != "null" THEN type ELSE 0 END as user_liked
          FROM reactions) as ul'), 'p.id', '=', 'ul.publication_id')
        ->groupBy('utilisateurs.image_profile', 'p.id', 'p.texte', 'us.name', 'p.created_at')
        ->orderByDesc('p.id')
        ->select('utilisateurs.image_profile', 'us.id as id_user',
                 'p.id', 
                 'p.texte', 
                 'us.name',
                 'p.created_at',
                 DB::raw('COUNT(DISTINCT r.id) as likes'),
                 DB::raw('COUNT(DISTINCT c.id) as comments_count'),
                 DB::raw('COUNT(DISTINCT i.id) as images_count'),
                 DB::raw('JSON_OBJECTAGG(IFNULL(i.id, \'\'), IFNULL(i.image, \'\')) as images'),
                 DB::raw('GROUP_CONCAT(DISTINCT CONCAT(u1.image_profile, \':\', liker.name, \':\', r.type)) as reactions'),
                 DB::raw('GROUP_CONCAT(DISTINCT r.type) as reaction_types'),
                 DB::raw('JSON_OBJECTAGG(IFNULL(c.id, \'\'), CONCAT(u2.image_profile,  \':\', commenter.name, \':\', commenter.id, \':\', c.texte)) as comments'),
                 DB::raw('MAX(IFNULL(ul.user_liked, \'\')) as user_liked'))
        ->where('p.id',$request->id)
        ->get();
        foreach ($publications as $publication) {
            $publication_created_at = Carbon::createFromFormat('Y-m-d H:i:s', $publication->created_at);
            $diff_in_days = $publication_created_at->diffInDays();
        
            // Vérifie si la publication a été créée il y a plus de 5 jours
            if ($diff_in_days > 5) {
                $publication->time_diff_human = $publication_created_at->format('j F Y');
            } else {
                $publication->time_diff_human = $publication_created_at->diffForHumans();
            }
        }
        
        return response()->json($publications);
            
 }
}