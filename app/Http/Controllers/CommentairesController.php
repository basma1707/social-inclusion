<?php

namespace App\Http\Controllers;

use App\Models\commentaires;
use App\Models\notification;
use App\Models\publications;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CommentairesController extends Controller
{
    public function AddComment(Request $request)
    {
        $user_id= Auth::id();
        $commentaires =new commentaires;
        $commentaires->user_id  =$user_id    ;
        $commentaires->publication_id =$request->PubId ; 
        $commentaires->texte =$request->message ; 
        $commentaires->save();
        
        $pub=publications::find($request->PubId);
  if($user_id!=$pub->user_id )
  {
    $notification =new notification;
    $notification->id_user1 =$user_id;     ;
    $notification->id_user2  =$pub->user_id  ; 
    $notification->notification ='commented your publication' ; 
    $notification->type ='comment' ; 
    $notification->vu ='non' ; 
    $notification->idpub =$request->PubId  ; 
    $notification->save();
  }
        
        return response()->json( );
    }
    public function DeleteComment(Request $request)
    {
        $Comment_id = $request->data;
        commentaires::where('id', $Comment_id)
        ->delete();
        return response()->json();
    }

    public function UpdatetComment(Request $request)
    {

        commentaires::where('id', $request->idComment)
                ->update(['texte' => $request->updatedCommentValue]);
        return response()->json();
    }
}
