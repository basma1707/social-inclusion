<?php

namespace App\Http\Controllers;

use App\Models\event;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class EventController extends Controller
{
    public function AddEvent(Request $request)
    {
        
        $validator = Validator::make($request->all(), [
     
            'nom' =>  'required|max:20',
            'description' => 'max:350',
            'date' => 'max:350',
            'latitude' => 'max:350',
            'longitude' => 'max:350',

        ]);
        if($validator->fails()){
            return response()->json(['error' =>$validator->errors()],400);
         }
         $imageName=null;
         if($request->file('image')!=null)
         {
            $image = $request->file('image');
            $imageName = uniqid();
            $imageName.= '.' . $image->getClientOriginalExtension();
            $image->move(public_path('images/event'), $imageName);
         }
         $user_id = Auth::id();
         $utilisateur = event::create(array_merge(
            $validator->validate(),
            ['image' => $imageName,'id_user'=>$user_id]
        ));

    }

    public function getEvent()
    {
        $events = DB::table('events')
    ->leftJoin('event_membres', 'event_membres.id_events', '=', 'events.id')
    ->leftJoin('utilisateurs', 'utilisateurs.user_id', '=', 'event_membres.id_user')
    ->select('events.*', 
        DB::raw('GROUP_CONCAT(utilisateurs.image_profile) AS participants'), 
        DB::raw('DATE_FORMAT(events.date, "%d") AS day'), 
        DB::raw('DATE_FORMAT(events.date, "%b") AS month'), 
        DB::raw('DATE_FORMAT(events.date, "%Y") AS year')
    )
    ->groupBy('events.id')
    ->orderBy('date', 'desc')
    ->get();

return response()->json($events);
 
    }
   
    public function ShowEvent(Request $request)
    {
      
        $user = DB::table('events')
        ->leftJoin('event_membres', 'event_membres.id_events', '=', 'events.id')
        ->select('events.*', DB::raw('count(event_membres.id) as nb_members'))
        ->where('events.id', $request->id)
        ->groupBy('events.id')
        ->get();

    
    if ($user->isEmpty()) {
        return response()->json(['error']);
    } else {
        return response()->json($user);
    }
    
    }  
    public function getEventinformation(Request $request)
    {
        $group = DB::table('events')
        ->where('id', $request->id)
        ->get();
        return response()->json($group);
    } 

}
