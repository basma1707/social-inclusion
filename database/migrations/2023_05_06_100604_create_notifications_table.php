<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notifications', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('id_user1');
            $table->unsignedBigInteger('id_user2');
            $table->string('notification')->nullable();
            $table->string('type')->nullable();
            $table->string('vu')->nullable();
            $table->unsignedBigInteger('idpub')->nullable();
            $table->timestamps();
            $table->foreign('id_user1')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade'); 
            $table->foreign('id_user2')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade'); 
            $table->foreign('idpub')->references('id')->on('publications')->onDelete('cascade')->onUpdate('cascade'); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notifications');
    }
}
