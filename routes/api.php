<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\eventMembre;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\userManagement;
use App\Http\Controllers\EventController;
use App\Http\Controllers\GroupController;
use App\Http\Controllers\AmitieController;
use App\Http\Controllers\ReactionController;
use App\Http\Controllers\PublicationController;
use App\Http\Controllers\UtilisateurController;
use App\Http\Controllers\CommentairesController;
use App\Http\Controllers\GroupMembresController;
use App\Http\Controllers\NotificationController;
use App\Http\Controllers\ResetPasswordController;
use App\Http\Controllers\ChangePasswordController;
use App\Http\Controllers\ChatController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('messages', [\App\Http\Controllers\ChatController::class, 'message']);
/*Route::group([
    'middleware' => 'api',  
], function ($router) {*/
    /*-----------------------Auth-------------------------------------------*/
    Route::post('login', [AuthController::class, 'login'])->name('login');
    Route::post('logout', [AuthController::class, 'logout']);
    Route::post('refresh', [AuthController::class, 'refresh']);
    Route::post('me', [AuthController::class, 'me']);   
    Route::post('getusers', [AuthController::class, 'getusers']); 
    Route::post('register', [AuthController::class, 'register']);
    Route::post('sendPasswordResetLink', [ResetPasswordController::class, 'sendEmail']);
    Route::post('resetPassword', [ChangePasswordController::class, 'process']);
    Route::post('addaccountdetails', [UtilisateurController::class, 'create']);
    /*-----------------------Profile-------------------------------------------*/
    Route::post('updateaccountdetails', [UtilisateurController::class, 'update']);
    Route::post('GetUserProfile', [UtilisateurController::class, 'showProfileUser']);
    Route::post('resetPasswordfromprofile', [AuthController::class, 'updatePassword']);
    Route::post('ShowPublicationProfile', [PublicationController::class, 'ShowPublicationProfile']);
    Route::post('ShowPublicationProfileuser', [PublicationController::class, 'ShowPublicationProfileuser']);
     /*-----------------------Publication-------------------------------------------*/
    Route::post('addPublication', [PublicationController::class,'Add']);
    Route::post('ShowPublication', [PublicationController::class,'show']);
    Route::post('DeletePost', [PublicationController::class,'DeletePost']);
    Route::post('PostNotification', [PublicationController::class,'PostNotification']);

    
    /*-----------------------Recherche-------------------------------------------*/
    Route::get('search', [UtilisateurController::class, 'search']);
    /*-----------------------Relation Amitié-------------------------------------------*/
    Route::post('ShowDemmande', [AmitieController::class,'show']);
    Route::post('acceptFriend', [AmitieController::class,'accept']);
    Route::post('DeleteFriend', [AmitieController::class,'Delete']);
    Route::post('AddFriend', [AmitieController::class,'Add']);
    Route::post('isFriend', [AmitieController::class,'isFriend']);
    Route::post('deleteRequestFriend', [AmitieController::class,'deleteRequestFriend']);
    /*-----------------------React-------------------------------------------*/
    Route::post('AddReact', [ReactionController::class,'AddReact']);
    Route::post('UpdateReact', [ReactionController::class,'UpdateReact']);
    Route::post('DeleteReact', [ReactionController::class,'DeleteReact']);
   /*-----------------------Comment-------------------------------------------*/
   Route::post('AddComment', [CommentairesController::class,'AddComment']);
   Route::post('DeleteComment', [CommentairesController::class,'DeleteComment']);
   Route::post('UpdatetComment', [CommentairesController::class,'UpdatetComment']);
/*-----------------------Group-------------------------------------------*/
      Route::post('AddGroup', [GroupController::class,'AddGroup']);
      Route::post('DeleteGroup', [GroupController::class,'DeleteGroup']);
      Route::post('UpdateGroup', [GroupController::class,'UpdateGroup']);
      Route::post('Showgroupe', [GroupController::class,'Showgroupe']);
      Route::post('isInGroup', [GroupMembresController::class,'isInGroup']);
      Route::post('AddMembre', [GroupMembresController::class,'AddMembre']);
      Route::post('RemoveMembre', [GroupMembresController::class,'RemoveMembre']);
      Route::post('getGroupinformation', [GroupController::class,'getGroupinformation']);
      Route::post('getGroups', [GroupController::class,'getGroups']);
  
/*-----------------------Event-------------------------------------------*/
Route::post('AddEvent', [EventController::class,'AddEvent']);
Route::post('getEvent', [EventController::class,'getEvent']);
Route::post('ShowEvent', [EventController::class,'ShowEvent']);
Route::post('isInEvent', [eventMembre::class,'isInEvent']);
Route::post('AddMembreEvent', [eventMembre::class,'AddMembreEvent']);
Route::post('RemoveMembreEvent', [eventMembre::class,'RemoveMembreEvent']);
Route::post('getEventinformation', [EventController::class,'getEventinformation']);


/*-----------------------Notification-------------------------------------------*/
Route::post('ShowNotification', [NotificationController::class,'show']);
Route::post('ShowallNotification', [NotificationController::class,'showall']);
Route::post('vu', [NotificationController::class,'vu']);
Route::post('delete', [NotificationController::class,'delete']);

/*-----------------------Admin-------------------------------------------*/
Route::post('getusersforAdmin', [userManagement::class,'getGusers']);
Route::post('GetContact', [UtilisateurController::class,'GetContact']);

/*});*/
